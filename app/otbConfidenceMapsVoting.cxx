
/*def d'une appli*/
#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"


#include "otbImageListToVectorImageFilter.h"
#include "otbMultiToMonoChannelExtractROI.h"
#include "otbImageList.h"

#include "otbConfidenceMapsVoting.h"

namespace otb{

namespace Wrapper
{

class ConfidenceMapsVoting: public Application
{
public:
  /** Standard class typedefs. */
  typedef ConfidenceMapsVoting Self;
  typedef Application Superclass;

  typedef itk::SmartPointer<Self> Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Filters typedef for concatenation */
  typedef otb::ImageList<FloatImageType> ImageListType;
  typedef ImageListToVectorImageFilter<ImageListType,
  				       FloatVectorImageType> ListConcatenerFilterType;
  
  typedef MultiToMonoChannelExtractROI<FloatVectorImageType::InternalPixelType,
                                       FloatImageType::PixelType>               ExtractROIFilterType;
  typedef ObjectList<ExtractROIFilterType>                                      ExtractROIFilterListType;


 /** Standard macro */
  itkNewMacro(Self);
  itkTypeMacro(ConfidenceMapsVoting, otb::Application);
  
  using FunctorType = otb::MajorityVoteConfidenceFunctor<FloatVectorImageType::PixelType>;
  using FilterType = otb::BinaryFunctorConfVotingImageFilterWithNBands<FloatVectorImageType,
								FloatVectorImageType,
								FunctorType>;
private:
  void DoInit()
  {
    SetName("ConfidenceMapsVoting");
    SetDescription("");

    AddParameter(ParameterType_InputImageList, "il", "Input classifications");
    SetParameterDescription( "il", "List of input classification maps to fuse. Labels in each classification image must represent the same class." );
    AddParameter(ParameterType_InputImageList, "confmaps", "Input confidence map");
    SetParameterDescription( "confmaps", "List of input confidence maps to fuse. IN same order than the list of input classification maps." );

    AddParameter(ParameterType_OutputImage,"out","The output classification image");
    SetParameterDescription("out","The output classification image resulting from the fusion of the input classification images.");
    SetDefaultOutputPixelType("out",ImagePixelType_uint8);

    AddParameter(ParameterType_OutputImage,"ordconf","The N first confidence map");
    SetParameterDescription("ordconf","The output classification image resulting from the fusion of the input classification images.");
    SetDefaultOutputPixelType("ordconf",ImagePixelType_double);
    MandatoryOff("ordconf");

    AddParameter(ParameterType_OutputImage,"ordlab","A multiband label image ordered with confidence voting ");
    SetParameterDescription("ordlab","The output classification image resulting from the fusion of the input classification images.");
    SetDefaultOutputPixelType("ordlab",ImagePixelType_uint8);    
    MandatoryOff("ordlab");

    AddParameter(ParameterType_Int,"nbclass","The number of possible class");
    MandatoryOff("nbclass");
    SetDefaultParameterInt("nbclass",23);
    
    AddParameter(ParameterType_Float,"eps","The epsilon value for the confidence marge decision vote");
    MandatoryOff("eps");
    SetDefaultParameterFloat("eps",0.01);
    
    AddParameter(ParameterType_Int,"nodecisionlabel","The label value for no decision in vote");
    MandatoryOff("nodecisionlabel");
    SetDefaultParameterInt("nodecisionlabel",100);
  }//end of DoInit

  void DoUpdateParameters()
  {
    // Nothing to do here : all parameters are independent
    m_Concatener = ListConcatenerFilterType::New();
    m_ImageList = ImageListType::New();
    m_ExtractorList = ExtractROIFilterListType::New();
  }//end of DoUpdateParameters


  void DoExecute()
  {

    FloatVectorImageListType::Pointer imageList = GetParameterImageList("il");
    FloatVectorImageListType::Pointer confList = GetParameterImageList("confmaps");
    if( imageList->Size() != confList->Size()  ) //Modifier pour tester taille des deux listes
      {
    	itkExceptionMacro("The two inputs list not have same size");
       }
    //std::cout << imageList->Size() << "\n";

    // Classification maps
    imageList->GetNthElement(0)->UpdateOutputInformation();
    m_ImageList = ImageListType::New();
    m_Concatener =  ListConcatenerFilterType::New();
    //Conf maps
    m_confList = ImageListType::New();
    m_ConcatenerConf =  ListConcatenerFilterType::New();
    for(unsigned int i=0; i<imageList->Size(); i++)
      {
      	FloatVectorImageType::Pointer image = imageList->GetNthElement(i);
	FloatVectorImageType::Pointer confmap = confList->GetNthElement(i);
	image->UpdateOutputInformation();
	confmap->UpdateOutputInformation();

      	ExtractROIFilterType::Pointer converter = ExtractROIFilterType::New();
	ExtractROIFilterType::Pointer converterConf = ExtractROIFilterType::New();
	converter->SetInput(image);
	converterConf->SetInput(confmap);
	converter->SetChannel(1);
	converterConf->SetChannel(1);
	converter->UpdateOutputInformation();
	converterConf->UpdateOutputInformation();
	m_ExtractorList->PushBack(converter);
	m_ExtractorList->PushBack(converterConf);
	m_ImageList->PushBack(converter->GetOutput());
	m_confList->PushBack(converterConf->GetOutput());
      }	

    m_Concatener->SetInput( m_ImageList );
    m_ConcatenerConf->SetInput( m_confList);

    /** Begin Fusion Step*/
    unsigned int nb_out_bands = GetParameterInt("nbclass");
    float epsilon = GetParameterFloat("eps");
    // Fusion filter
    filter = FilterType::New();
    
    filter->SetInput(0, m_Concatener->GetOutput());
    filter->SetInput(1, m_ConcatenerConf->GetOutput());
    filter->SetNumberOfOutputBands(nb_out_bands);
    filter->SetEpsilon(epsilon);
    filter->SetNoDecisionLabel(GetParameterInt("nodecisionlabel"));
    filter->UpdateOutputInformation();
    SetParameterOutputImage("out",filter->GetOutput());
    SetParameterOutputImage("ordlab",filter->GetOutputOrderedLabel());
    SetParameterOutputImage("ordconf",filter->GetOutputConfidence());
    

  }
  ListConcatenerFilterType::Pointer  m_Concatener;
  ListConcatenerFilterType::Pointer  m_ConcatenerConf;
  ExtractROIFilterListType::Pointer  m_ExtractorList;
  FilterType::Pointer filter;
  ImageListType::Pointer        m_ImageList;
  ImageListType::Pointer        m_confList;
};// end class FusionClassification


}// end namespace Wrapper
}//end namespace otb
OTB_APPLICATION_EXPORT(otb::Wrapper::ConfidenceMapsVoting)
