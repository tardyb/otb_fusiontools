
#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbImageListToVectorImageFilter.h"
#include "otbMultiToMonoChannelExtractROI.h"
#include "otbImageList.h"
#include <boost/algorithm/string.hpp>

#include "otbProbabilityMapsVoting.h"

namespace otb
{
namespace Wrapper
{

class ProbabilityMapsVoting: public Application
{
public:
  /** Standard class typedefs. */
  typedef ProbabilityMapsVoting Self;
  typedef Application Superclass;

  typedef itk::SmartPointer<Self> Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Filters typedef for concatenation */
  typedef otb::ImageList<FloatImageType> ImageListType;
  typedef ImageListToVectorImageFilter<ImageListType,
  				       FloatVectorImageType> ListConcatenerFilterType;
  
  typedef MultiToMonoChannelExtractROI<FloatVectorImageType::InternalPixelType,
                                       FloatImageType::PixelType>               ExtractROIFilterType;
  typedef ObjectList<ExtractROIFilterType>                                      ExtractROIFilterListType;


 /** Standard macro */
  itkNewMacro(Self);
  itkTypeMacro(ProbabilityMapsVoting, otb::Application);
  
  using FunctorType = otb::MajorityProbabilityVotingFunctor<FloatVectorImageType::PixelType>;
  using FilterType = otb::BinaryFunctorProbaVotingImageFilterWithNBands<FloatVectorImageType,
								FloatVectorImageType,
								FunctorType>;
private:
  void DoInit()
  {
    SetName("ProbabilityMapsVoting");
    SetDescription("");

    AddParameter(ParameterType_InputImageList, "il", "Input probabilities maps one per class ordered by class id");
    SetParameterDescription( "il", "List of input proba maps to fuse. The list must be sorted by class id." );
    
    //AddParameter(ParameterType_InputImageList, "confmaps", "Input confidence map");
    //SetParameterDescription( "confmaps", "List of input confidence maps to fuse. IN same order than the list of input classification maps." );

    AddParameter(ParameterType_OutputImage,"out","The output classification image");
    SetParameterDescription("out","The output classification image resulting from the fusion of the input classification images.");
    SetDefaultOutputPixelType("out",ImagePixelType_uint8);

    AddParameter(ParameterType_String, "nomen", "Nomenclature CSV file");
    SetParameterDescription("nomen", "CSV file");

    AddParameter(ParameterType_Float,"eps","The epsilon value for the confidence marge decision vote");
    MandatoryOff("eps");
    SetDefaultParameterFloat("eps",0.01);

    
    AddParameter(ParameterType_Int,"nodecisionlabel","The label value for no decision in vote");
    MandatoryOff("nodecisionlabel");
    SetDefaultParameterInt("nodecisionlabel", 100);
  }//end of DoInit

  void DoUpdateParameters()
  {
    // Nothing to do here : all parameters are independent
    m_Concatener = ListConcatenerFilterType::New();
    m_ImageList = ImageListType::New();
    m_ExtractorList = ExtractROIFilterListType::New();
  }//end of DoUpdateParameters


  void DoExecute()
  {

    FloatVectorImageListType::Pointer imageList = GetParameterImageList("il");
    unsigned int nbImages = imageList->Size();
    // Recuperer les labels dans un vecteur puis verifier qu'il y a autant de classe que d'image
    std::string in_nomen_file{""};
    in_nomen_file = GetParameterString("nomen");
    std::ifstream csvFile(in_nomen_file);
    if(!csvFile)
      itkGenericExceptionMacro(<<"Could not open file"<<in_nomen_file<<"\n");
    std::string ligne;
    int nbLignes = 0;
    while(std::getline(csvFile,ligne))
      nbLignes++;
    csvFile.close();
    nbLignes--; // supprimer la ligne des non decisions
    FloatVectorImageType::Pointer firstImage = imageList->GetNthElement(0);
    firstImage->UpdateOutputInformation();
    unsigned int nbBand = firstImage->GetNumberOfComponentsPerPixel();
    for(unsigned int i = 1; i<nbImages; i++)
      {
	FloatVectorImageType::Pointer currentImage = imageList->GetNthElement(i);
	currentImage->UpdateOutputInformation();
	if( currentImage->GetNumberOfComponentsPerPixel() != nbBand || nbBand != nbLignes ) //Modifier pour tester taille des deux listes
	  {
	    std::cout << nbBand << "," << nbLignes << "\n";
	    itkExceptionMacro("The number of band in proba map images must be the same as the number of classes in csvFile (-1 for no decision class at last line)");
	  }	
      }


    int idx = 0;
    std::ifstream csvFile2(in_nomen_file);
    std::vector<int> labels(nbLignes);
    char delim = '\n';
    for(int i = 0; i<=nbLignes; ++i)
      {
    	std::getline(csvFile2,ligne,delim);
    	std::vector<std::string> chaine;
    	boost::split(chaine,ligne,boost::is_any_of(":"));
    	labels[idx] = stoi(chaine[1]);
    	idx++;
      }
    csvFile2.close();
    
    // Classification maps
    imageList->GetNthElement(0)->UpdateOutputInformation();
    m_ImageList = ImageListType::New();
    m_Concatener =  ListConcatenerFilterType::New();
    for(unsigned int i=0; i<imageList->Size(); i++)
      {
      	FloatVectorImageType::Pointer image = imageList->GetNthElement(i);
	image->UpdateOutputInformation();
	for(unsigned int j = 0; j < nbBand; j++)
	  {
	    ExtractROIFilterType::Pointer converter = ExtractROIFilterType::New();
	    converter->SetInput(image);
	    converter->SetChannel(j+1);
	    converter->UpdateOutputInformation();
	    m_ExtractorList->PushBack(converter);
	    m_ImageList->PushBack(converter->GetOutput());
	  }	
      }

    m_Concatener->SetInput( m_ImageList );

    /** Begin Fusion Step*/

    float epsilon = GetParameterFloat("eps");
    // Fusion filter
    filter = FilterType::New();
    
    filter->SetInput(0, m_Concatener->GetOutput());
    filter->SetNumberOfInputBands(nbBand);
    filter->SetEpsilon(epsilon);
    filter->SetLabels(labels);
    filter->SetNoDecisionLabel(GetParameterInt("nodecisionlabel"));
    filter->UpdateOutputInformation();
    SetParameterOutputImage("out",filter->GetOutput());
    
  }
  ListConcatenerFilterType::Pointer  m_Concatener;
  ExtractROIFilterListType::Pointer  m_ExtractorList;
  FilterType::Pointer filter;
  ImageListType::Pointer        m_ImageList;
  
};// end class FusionClassification


}// end namespace Wrapper
}//end namespace otb
OTB_APPLICATION_EXPORT(otb::Wrapper::ProbabilityMapsVoting)
