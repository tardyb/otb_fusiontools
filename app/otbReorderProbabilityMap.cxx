/*def d'une appli*/
#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbImageListToVectorImageFilter.h"
#include "otbMultiToMonoChannelExtractROI.h"
#include "otbImageList.h"
#include "otbFunctorImageFilter.h"
//#include "otbVariadicNamedInputsImageFilter.h"

#include "otbReorderProbabilityMap.h"


namespace otb
{
namespace Wrapper
{
class ReorderProbabilityMap: public Application
{
public:
  typedef ReorderProbabilityMap Self;
  typedef Application Superclass;
  /** Filters typedef for concatenation */
  typedef UInt32VectorImageType                VectorImageType;
  typedef UInt32ImageType                      IOLabelImageType;
  typedef IOLabelImageType::InternalPixelType  LabelPixelType;

  typedef std::vector<unsigned int> LabelsVectorType;

   /** Standard macro */
  itkNewMacro(Self);
  itkTypeMacro(ReorderProbabilityMap, otb::Application);

  using ReorderFunctorType = ReorderImageByLabel<LabelPixelType, LabelPixelType>;
private:
  void DoInit()
  {
    SetName("ReorderProbabilityMap");
    SetDescription("Change bands for Probability Maps");
    SetDocLongDescription("This application use the class dictionnary from Shark RFF classifier, an external file which contains a nomenclature and the output Probability Map from ImageClassifier. The output image is an ordered by label probability image following the increase label value for nomenclature file. Missing labels in the class dictionnary are filled with 0 value.");


    AddParameter(ParameterType_InputImage, "in", "The probability map");
    SetParameterDescription("in", "The input probability map");

    AddParameter(ParameterType_String, "model", "The model used to generate the map.");
    SetParameterDescription("model", "Model from TrainVectorClassifier or TrainImageClassifier. It must be contains a class dictionnary");

    AddParameter(ParameterType_String, "nomen", "The nomenclature file");
    SetParameterDescription("nomen", "The nomenclature file. A text file (.txt or .csv) containing the labels, one by row. Comments row must start with # and the first row after # must be the nodata label.");
    
    AddParameter(ParameterType_OutputImage,"out","The output reoreder image");
    SetParameterDescription("out","The output map");
  }

  void DoUpdateParameters()
  {
    // Nothing to do here.
  }



  auto getSortedLabelsFromModel(const std::string & filename)
  {
    // The class dictionnary is already sorted by increasing label value
    std::vector<unsigned int> out;
    std::ifstream ifs(filename);
    if (ifs.good())
      {
	// Check if the first line is a comment and verify the name of the model in this case.
	std::string line;
	getline( ifs, line );
	if( line.at( 0 ) == '#' )
	  {
	    if( line.find( "RFClassifier" ) == std::string::npos )
	      itkExceptionMacro( "The model file : " + filename + " cannot be read." );
	    if( line.find( "with_dictionary" ) == std::string::npos )
	      {
		itkExceptionMacro("The model file : " + filename + "has not class dictionnary");
	      }
	  }
      }
    else
      {
      // rewind if first line is not a comment
      ifs.clear();
      ifs.seekg( 0, std::ios::beg );
      }
    size_t nbLabels{0};
    ifs >> nbLabels;
    out.resize(nbLabels);
    for(size_t i=0; i<nbLabels; ++i)
      {
	unsigned int label;
	ifs >> label;
	out[i] = label;
      }
    return out;
  }

  std::vector<unsigned int> getSortedLabelsFromNomen(const std::string & filename)
  {
    std::vector<unsigned int> out;
    std::ifstream ifs(filename);
    if(!ifs)
      itkGenericExceptionMacro(<< "Could not open file " << filename << "\n");
    std::string line;
    getline(ifs, line);
    while(line.at(0) == '#')
      {
	getline(ifs, line);
      }
    // skip the first not commented line
    getline(ifs,line);
    getline(ifs, line);
    std::string classLine("");
    while(ifs>>classLine)
      {
	out.push_back(std::stoi(classLine));
      }

    std::sort(std::begin(out), std::end(out));
    return out;
  }
  void DoExecute()
  {
    UInt32VectorImageType::Pointer inImage = GetParameterUInt32VectorImage("in");
    inImage->UpdateOutputInformation();
    auto modelName = GetParameterString("model");
    auto nomenName = GetParameterString("nomen");
    LabelsVectorType originLabels = getSortedLabelsFromNomen(nomenName);
    LabelsVectorType modelLabels = getSortedLabelsFromModel(modelName);
    //otbAppLogINFO("Nomenclature Labels : " << originLabels);
    //otbAppLogINFO("Found Labels in model : " << modelLabels);
    
    
    // Call the functor
    auto reorder = NewFunctorFilter(ReorderFunctorType{originLabels, modelLabels});
    reorder->SetInput1(inImage);
    reorder->Update();
    SetParameterOutputImage("out", reorder->GetOutput());
    // Write the output
  }
}; //end of ReorderProbabilityMaps
} // end of namespace Wrapper
} // end of namespace OTB
OTB_APPLICATION_EXPORT(otb::Wrapper::ReorderProbabilityMap)
