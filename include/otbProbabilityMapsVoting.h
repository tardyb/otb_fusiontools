#ifndef _otbFusionProba_h
#define _otbFusionProba_h

#include "itkImageToImageFilter.h"
#include "itkVariableLengthVector.h"


namespace otb
{

  template <typename KeyType, typename ValuesType>
  struct Compare
    {
      typedef typename std::pair<KeyType,ValuesType> PairType;
      bool operator()(PairType x,PairType y)
      {
	return x.second>y.second;
      }
    };
  /**Functor */

  template <typename PixelType>
class MajorityProbabilityVotingFunctor
{
public:
  using LabelVectorType = typename std::vector<int>;
  using ValueType = typename PixelType::ValueType;
  using VectorType = typename std::vector<ValueType>;
  using MapType = typename std::map<ValueType,ValueType>;
  using MapTypeIt = typename MapType::const_iterator;
  using TupleType = typename std::tuple<PixelType,PixelType,PixelType>;
  
  MajorityProbabilityVotingFunctor() = default;
  
  PixelType operator()(PixelType proba, LabelVectorType vectLabels, float epsilon,unsigned int nbInputBand, unsigned int nodecisionlabel) const
  {
    auto nbCartes = proba.GetSize()/nbInputBand;
    
    MapType VoteMaj;
    for(size_t j = 0; j < nbInputBand; j++)
      {
	double sum = 0.0;
	for(size_t i = 0;i<nbCartes;i++)
	  {
	    sum += proba[j+i*nbInputBand];
	  }
	VoteMaj[vectLabels[j]] = sum;
      }
    
    // Initialize output pixel, 0 is the non decision label
    PixelType labeled{1};
    labeled.Fill(nodecisionlabel);
    if (VoteMaj.size() > 1)
      {
	// convert map into std::vector for sorting
	std::vector<std::pair<ValueType,ValueType>> VectorVote(VoteMaj.size());
	std::copy(VoteMaj.begin(),VoteMaj.end(),VectorVote.begin());
	std::sort(VectorVote.begin(),VectorVote.end(),Compare<ValueType,ValueType>());
    
	ValueType v1 = VectorVote[0].second;
	ValueType v2 = VectorVote[1].second;
	// Looking for strickly maximum (with epsilon)
	
	if ( (v1-v2) > epsilon*1000)
	  {
	    //Abble to take a decision
	    labeled[0] = VectorVote[0].first;	    
	  }

      }
    else
      {
	// if only one class, return the label and the confiance
	labeled[0] = VoteMaj.begin()->first;
      }
  return labeled;
  }
 }; //End functor

  /** Filter */

template <class TInputImage ,class TOutputImage, class TFunctor>
class ITK_EXPORT BinaryFunctorProbaVotingImageFilterWithNBands : 
    public itk::ImageToImageFilter< TInputImage, TOutputImage>
{
 public:
  typedef BinaryFunctorProbaVotingImageFilterWithNBands Self;
  typedef itk::ImageToImageFilter<TInputImage, TOutputImage> Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;
  typedef TFunctor FunctorType;
  
  /** Method for creation through the object factory. */
  itkNewMacro(Self);
  /** Macro defining the type*/
  itkTypeMacro(BinaryFunctorProbaVotingImageFilterWithNBands, SuperClass);

  /** Pointer for threading */
  typedef TInputImage                                InputImageType;
  typedef typename InputImageType::ConstPointer      InputImageConstPointerType;
  typedef typename InputImageType::InternalPixelType ValueType;
  typedef typename InputImageType::PixelType         PixelType;
  
  typedef TOutputImage                               OutputImageType;
  typedef typename OutputImageType::Pointer          OutputImagePointerType;
  typedef typename OutputImageType::RegionType       OutputImageRegionType;
  typedef typename OutputImageType::PixelType        LabelType;  
  
  typedef otb::VectorImage<double>                         ConfidenceImageType;
  typedef typename ConfidenceImageType::Pointer      ConfidenceImagePointerType;

  typedef OutputImageType                   OrderedLabelImageType;
  typedef typename OrderedLabelImageType::Pointer    OrderedLabelImagePointerType;
  
  typedef typename std::vector<int>                  LabelVectorType;

  /** Accessors for the number of bands*/
  itkSetMacro(NumberOfOutputBands, unsigned int);
  itkGetConstMacro(NumberOfOutputBands, unsigned int);

  itkSetMacro(Epsilon,double);
  itkGetConstMacro(Epsilon,double);

  itkSetMacro(NumberOfInputBands, unsigned int);
  itkGetMacro(NumberOfInputBands, unsigned int);

  itkSetMacro(NoDecisionLabel, unsigned int);
  itkGetMacro(NoDecisionLabel, unsigned int);

  void SetLabels(LabelVectorType labels)
  {
    m_Labels.resize(labels.size());
    std::copy(labels.begin(),labels.end(),m_Labels.begin());
  }
  /**
   * Get the output confidence map
   */
  ConfidenceImageType * GetOutputConfidence(void);
  // Get the output ordered label
  OrderedLabelImageType * GetOutputOrderedLabel(void);
 protected:
  /** Constructor */
  BinaryFunctorProbaVotingImageFilterWithNBands();
  /** Destructor */
  virtual ~BinaryFunctorProbaVotingImageFilterWithNBands(){}
  
  /** Threaded generate data */
  virtual void ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, itk::ThreadIdType threadId);
  
  void GenerateOutputInformation()
  {
    Superclass::GenerateOutputInformation();
    // Define the number of output bands
    this->GetOutput()->SetNumberOfComponentsPerPixel(1);
  }

 private:
  BinaryFunctorProbaVotingImageFilterWithNBands(const Self &); //purposely not implemented
  void operator =(const Self&); //purposely not implemented

  unsigned int m_NumberOfOutputBands;
  unsigned int m_NumberOfInputBands;
  unsigned int m_NoDecisionLabel;
  TFunctor m_Functor;
  LabelVectorType m_Labels;
  double m_Epsilon;
};// End of BinaryFunctorImageFilterWithNBands

}//End namespace fusion

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbProbabilityMapsVoting.hxx"
#endif

#endif
