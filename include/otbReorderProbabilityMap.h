#ifndef __OTBREORDERPROBABILITYMAPS_H
#define __OTBREORDERPROBABILITYMAPS_H

namespace otb
{
 
template <typename TOut, typename TIn>
struct ReorderImageByLabel
{
  ReorderImageByLabel(std::vector<unsigned int> orderedLabel,
		      std::vector<unsigned int> probaLabel
		      ) : m_labels(orderedLabel),
			  m_problab(probaLabel)
  {}
  auto operator()(const itk::VariableLengthVector<TIn> &in)
  {
    itk::VariableLengthVector<TOut> out(m_labels.size());
    unsigned int cptLab = 0;
    unsigned int cptProb = 0;
    while (cptLab < m_labels.size() && cptProb < m_problab.size())
      {
	if (m_labels[cptLab] == m_problab[cptProb])
	  {
	    out[cptLab] = in[cptProb];
	    cptLab ++;
	    cptProb ++;
	  }
	else
	  {
	    out[cptLab] = 0;
	    cptLab ++;
	  }
      }
    for (size_t i = 0; i<out.Size(); i++)
      {
	std::cout<<out[i]<<" | ";
      }
    std::cout << std::endl;
    return out;
  }

  size_t OutputSize(...) const
  {
    return m_labels.size();
  }

  std::vector<unsigned int> m_labels;
  std::vector<unsigned int> m_problab;
};

  
}


#endif
