
#ifndef __otbConfidenceMapsVoting_hxx
#define __otbConfidenceMapsVoting_hxx

#include "otbConfidenceMapsVoting.h"
#include "itkImageRegionIterator.h"
#include "itkProgressReporter.h"

namespace otb{

/** Constructor */
template <class TInputImage ,class TOutputImage, class TFunctor>
BinaryFunctorConfVotingImageFilterWithNBands<TInputImage, TOutputImage, TFunctor>
::BinaryFunctorConfVotingImageFilterWithNBands()
{

  this->SetNthOutput(0,TOutputImage::New());
  this->SetNthOutput(1,ConfidenceImageType::New());
  this->SetNthOutput(2,OrderedLabelImageType::New());
}
  /**Outputs access */
template <class TInputImage ,class TOutputImage, class TFunctor>
typename BinaryFunctorConfVotingImageFilterWithNBands<TInputImage,TOutputImage, TFunctor>
::ConfidenceImageType *
BinaryFunctorConfVotingImageFilterWithNBands<TInputImage, TOutputImage, TFunctor>
::GetOutputConfidence()
{

  if (this->GetNumberOfOutputs() < 3)
    {
      return 0;
    }
  return static_cast<ConfidenceImageType *>(this->itk::ProcessObject::GetOutput(1));
}

template <class TInputImage ,class TOutputImage, class TFunctor>
typename BinaryFunctorConfVotingImageFilterWithNBands<TInputImage,TOutputImage, TFunctor>
::OrderedLabelImageType *
BinaryFunctorConfVotingImageFilterWithNBands<TInputImage, TOutputImage, TFunctor>
::GetOutputOrderedLabel()
{

  if (this->GetNumberOfOutputs() < 3)
    {
      return 0;
    }
  return static_cast<OrderedLabelImageType *>(this->itk::ProcessObject::GetOutput(2));
}

  /** Threading*/
template <class TInputImage ,class TOutputImage, class TFunctor>
void BinaryFunctorConfVotingImageFilterWithNBands<TInputImage, TOutputImage, TFunctor >
::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, itk::ThreadIdType threadId)
{

  /** Inputs pointers */
  InputImageConstPointerType inputPtr = this->GetInput(0);
  InputImageConstPointerType inputPtrConf = this->GetInput(1);
  OutputImagePointerType outputPtr = this->GetOutput(0);
  ConfidenceImagePointerType outputConfPtr = this->GetOutputConfidence();
  OrderedLabelImagePointerType outputOrdLabPtr = this->GetOutputOrderedLabel();

  // Progress Reporting 
  itk::ProgressReporter progress(this, threadId, outputRegionForThread.GetNumberOfPixels());

  // Define Iterators
  typedef itk::ImageRegionConstIterator<InputImageType> InputIteratorType;
  typedef itk::ImageRegionIterator<OutputImageType>     OutputIteratorType;
  typedef itk::ImageRegionIterator<ConfidenceImageType> ConfidenceMapIteratorType;
  typedef itk::ImageRegionIterator<OrderedLabelImageType> OrderedLabelIteratorType;

  InputIteratorType inIt(inputPtr, outputRegionForThread);
  InputIteratorType inConfIt(inputPtrConf, outputRegionForThread);
  OutputIteratorType outIt( outputPtr, outputRegionForThread);
  
  ConfidenceMapIteratorType confidenceIt(outputConfPtr, outputRegionForThread);
  OrderedLabelIteratorType orderedLabIt(outputOrdLabPtr, outputRegionForThread);
  
  for (inIt.GoToBegin(),inConfIt.GoToBegin(),outIt.GoToBegin(),confidenceIt.GoToBegin(),orderedLabIt.GoToBegin();
       !inIt.IsAtEnd() && !inConfIt.IsAtEnd() && !outIt.IsAtEnd() && !confidenceIt.IsAtEnd() && !orderedLabIt.IsAtEnd();
       ++inIt,++inConfIt,++outIt,++confidenceIt,++orderedLabIt)
    {

      std::tuple<PixelType,PixelType,PixelType> result_functor = m_Functor(inIt.Get(),inConfIt.Get(),m_Epsilon, m_NoDecisionLabel);
      PixelType label;
      ConfidenceImageType::PixelType ordConf;
      PixelType ordLab;
      std::tie(label,ordConf,ordLab) = result_functor;
      outIt.Set(label);
      confidenceIt.Set(ordConf);
      orderedLabIt.Set(ordLab);

    }
      
}
}// End namespace otb

#endif
