
#ifndef __otbFusionProba_hxx
#define __otbFusionProba_hxx

#include "otbProbabilityMapsVoting.h"
#include "itkImageRegionIterator.h"
#include "itkProgressReporter.h"

namespace otb{

/** Constructor */
template <class TInputImage ,class TOutputImage, class TFunctor>
BinaryFunctorProbaVotingImageFilterWithNBands<TInputImage, TOutputImage, TFunctor>
::BinaryFunctorProbaVotingImageFilterWithNBands()
{

  this->SetNthOutput(0,TOutputImage::New());
}

  /** Threading*/
template <class TInputImage ,class TOutputImage, class TFunctor>
void BinaryFunctorProbaVotingImageFilterWithNBands<TInputImage, TOutputImage, TFunctor >
::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, itk::ThreadIdType threadId)
{

  /** Inputs pointers */
  InputImageConstPointerType inputPtr = this->GetInput(0);
  OutputImagePointerType outputPtr = this->GetOutput(0);

  // Progress Reporting 
  itk::ProgressReporter progress(this, threadId, outputRegionForThread.GetNumberOfPixels());

  // Define Iterators
  typedef itk::ImageRegionConstIterator<InputImageType> InputIteratorType;
  typedef itk::ImageRegionIterator<OutputImageType>     OutputIteratorType;

  InputIteratorType inIt(inputPtr, outputRegionForThread);
  OutputIteratorType outIt( outputPtr, outputRegionForThread);
  
  for (inIt.GoToBegin(),outIt.GoToBegin();!inIt.IsAtEnd() && !outIt.IsAtEnd();++inIt,++outIt)
    {
      outIt.Set(m_Functor(inIt.Get(),m_Labels,m_Epsilon,m_NumberOfInputBands, m_NoDecisionLabel));
    }
      
}
}// End namespace otb

#endif
