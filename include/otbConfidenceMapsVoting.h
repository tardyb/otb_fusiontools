#ifndef _otbConfidenceMapsVoting_h
#define _otbConfidenceMapsVoting_h

#include "itkImageToImageFilter.h"
#include "itkVariableLengthVector.h"


namespace otb
{

  template <typename KeyType, typename ValuesType>
  struct Compare
    {
      typedef typename std::pair<KeyType,ValuesType> PairType;
      bool operator()(PairType x,PairType y)
      {
	return x.second>y.second;
      }
    };
  /**Functor */

  template <typename PixelType>
class MajorityVoteConfidenceFunctor
{
public:

  using ValueType = typename PixelType::ValueType;
  using VectorType = typename std::vector<ValueType>;
  using MapType = typename std::map<ValueType,ValueType>;
  using MapTypeIt = typename MapType::const_iterator;
  using TupleType = typename std::tuple<PixelType,PixelType,PixelType>;
  
  MajorityVoteConfidenceFunctor() = default;
  
  TupleType operator()(PixelType label,PixelType confiance, float epsilon, unsigned int nodecisionlabel) const
  {
    auto nbCartes = label.GetSize();

    MapType VoteMaj;
    for(size_t i=0;i<nbCartes;i++)
      {
	
	if(VoteMaj.find(label[i]) != VoteMaj.end())
	  {
	    VoteMaj[label[i]] += confiance[i];
	  }
	else
	  {
	    VoteMaj[label[i]] = confiance[i];
	  }
      }
    
    // Initialize output pixel with no decision label with a confidence value of 0
    PixelType labeled{1};
    labeled.Fill(nodecisionlabel);
    PixelType confOrd{nbCartes};
    confOrd.Fill(0.);
    PixelType labOrd{nbCartes};
    labOrd.Fill(nodecisionlabel);
    if (VoteMaj.size() > 1)
      {
	// convert map into std::vector for sorting
	std::vector<std::pair<ValueType,ValueType>> VectorVote(VoteMaj.size());
	std::copy(VoteMaj.begin(),VoteMaj.end(),VectorVote.begin());
	std::sort(VectorVote.begin(),VectorVote.end(),Compare<ValueType,ValueType>());
    
	ValueType v1 = VectorVote[0].second;
	ValueType v2 = VectorVote[1].second;
	// Looking for strickly maximum (with epsilon)
	//std::cout << (v1-v2)  << " epsilon" << epsilon*1000 << "\n";
	if ( (v1-v2) > epsilon)
	  {
	    //std::cout << "Votes " << v1 << "  " << v2 << " " << (v1-v2) << " " << epsilon << "\n";
	    //Able to take a decision
	    labeled[0] = VectorVote[0].first;	    
	  }

	// Complete output images for show the sorted values
	size_t idx{0};
	for(const auto& p : VectorVote)
	  {
	    confOrd[idx] = p.second;
	    labOrd[idx] = p.first;
	    ++idx;
	  }
      }
    else
      {
	// if only one class, return the label and the confiance
	labeled[0] = VoteMaj.begin()->first;
	confOrd[0] = VoteMaj.begin()->second;
	labOrd[0]=labeled[0];
      }

    TupleType output = std::make_tuple(labeled,confOrd,labOrd);
    return output;
  }
 }; //End functor

  /** Filter */

template <class TInputImage ,class TOutputImage, class TFunctor>
class ITK_EXPORT BinaryFunctorConfVotingImageFilterWithNBands : 
    public itk::ImageToImageFilter< TInputImage, TOutputImage>
{
 public:
  typedef BinaryFunctorConfVotingImageFilterWithNBands Self;
  typedef itk::ImageToImageFilter<TInputImage, TOutputImage> Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;
  typedef TFunctor FunctorType;
  
  /** Method for creation through the object factory. */
  itkNewMacro(Self);
  /** Macro defining the type*/
  itkTypeMacro(BinaryFunctorConfVotingImageFilterWithNBands, SuperClass);

  /** Pointer for threading */
  typedef TInputImage                                InputImageType;
  typedef typename InputImageType::ConstPointer      InputImageConstPointerType;
  typedef typename InputImageType::InternalPixelType ValueType;
  typedef typename InputImageType::PixelType         PixelType;
  
  typedef TOutputImage                               OutputImageType;
  typedef typename OutputImageType::Pointer          OutputImagePointerType;
  typedef typename OutputImageType::RegionType       OutputImageRegionType;
  typedef typename OutputImageType::PixelType        LabelType;  
  
  typedef otb::VectorImage<double>                         ConfidenceImageType;
  typedef typename ConfidenceImageType::Pointer      ConfidenceImagePointerType;

  typedef OutputImageType                   OrderedLabelImageType;
  typedef typename OrderedLabelImageType::Pointer    OrderedLabelImagePointerType;


  /** Accessors for the number of bands*/
  itkSetMacro(NumberOfOutputBands, unsigned int);
  itkGetConstMacro(NumberOfOutputBands, unsigned int);

  itkSetMacro(Epsilon,double);
  itkGetConstMacro(Epsilon,double);

  itkSetMacro(NoDecisionLabel, unsigned int);
  itkGetMacro(NoDecisionLabel, unsigned int);
  /**
   * Get the output confidence map
   */
  ConfidenceImageType * GetOutputConfidence(void);
  // Get the output ordered label
  OrderedLabelImageType * GetOutputOrderedLabel(void);
 protected:
  /** Constructor */
  BinaryFunctorConfVotingImageFilterWithNBands();
  /** Destructor */
  virtual ~BinaryFunctorConfVotingImageFilterWithNBands(){}
  
  /** Threaded generate data */
  virtual void ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, itk::ThreadIdType threadId);
  
  void GenerateOutputInformation()
  {
    Superclass::GenerateOutputInformation();
    // Define the number of output bands
    this->GetOutput()->SetNumberOfComponentsPerPixel(1);
    this->GetOutputConfidence()->SetNumberOfComponentsPerPixel(m_NumberOfOutputBands);
    this->GetOutputOrderedLabel()->SetNumberOfComponentsPerPixel(m_NumberOfOutputBands);
  }

 private:
  BinaryFunctorConfVotingImageFilterWithNBands(const Self &); //purposely not implemented
  void operator =(const Self&); //purposely not implemented

  unsigned int m_NumberOfOutputBands;
  unsigned int m_NoDecisionLabel;
  TFunctor m_Functor;
  double m_Epsilon;
};// End of BinaryFunctorImageFilterWithNBands

}//End namespace fusion

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbConfidenceMapsVoting.hxx"
#endif


#endif
